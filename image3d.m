function varargout = image3d(varargin)
  
  data = varargin{1};
  
  if nargin == 2
    switch upper(varargin{2});
      case 'XZ'
        data = permute(data, [1 3 2]);
      case 'YZ'
        data = permute(data, [2 3 1]);
    endswitch
  endif
    
  dims = size(data);
  m = ceil(sqrt(dims(3)));
  img = nan(dims(1) * m, dims(2) * m);
  
  for i = 1 : dims(3)
    ix = floor((i - 1) / m) * dims(1);
    iy = mod((i - 1), m) * dims(2);
    img(ix + 1 : ix + dims(1), iy + 1 : iy + dims(2)) = data(:, :, i);
  endfor
  
  if nargout == 0
    imagesc(img)
  else
    varargout{1} = img;
  endif
  
endfunction
##