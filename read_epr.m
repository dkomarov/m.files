function epri = read_epr(fname)
  
  idx = strfind(fname, "/");
  if idx
    dir = fname(1 : idx(end) - 1);
    name = fname(idx(end) + 1 : end);
  else
    dir = pwd();
    name = fname;
  endif
  
  ext = ".tar.gz";
  if length(name) > 7 && strcmpi(name(end - 6 : end), ext)
    name = name(1: end - 7);
  endif
  
  fullName = strcat(dir, "/", name, ext);
  rawDataDir = strcat(dir, "/", name);
  
  if isfolder(rawDataDir)
    unpacked_dir = rawDataDir;
    
  else
    if ~isfile(fullName)
      error("File \"%s\" does not exist\n", fullName)
    endif
    
    h = hash("md5", fileread(fullName));
    unpacked_dir = strcat("/tmp/epri/", h);
    
    if ~isfolder(unpacked_dir)
      mkdir(unpacked_dir);
      gunzip(fullName, unpacked_dir);  
    endif
  endif
  
  fl = fopen(strcat(unpacked_dir, "/", name, ".exp"), "r");

  while ~feof(fl)
    str = fgets(fl);
    
    if str(1) == '#'
      continue
    endif
    
    [varName, value] = sscanf(str, "%s = %f", "C");
    if strcmp(varName, "CENTER_FIELD")
      [varName, value] = sscanf(str, "%s = %*s%f", "C");
      epri.sets.cf = value;
    elseif strcmp(varName, "SWEEP_WIDTH")
      epri.sets.sw = value;
    elseif strcmp(varName, "SCAN_TIME")
      epri.sets.t1 = value;
    elseif strcmp(varName, "NUMBER_OF_POINTS")
      epri.sets.n = value;
    elseif strcmp(varName, "NUMBER_OF_PROJECTIONS")
      epri.sets.k3 = value;
    elseif strcmp(varName, "NUMBER_OF_SCANS")
      epri.sets.nScans = value;
    elseif strcmp(varName, "MAXIMUM_GRADIENT")
      epri.sets.maxGr = value;
    endif
    
  endwhile
  
  fclose(fl);
  
  if isfolder(rawDataDir)
    fl = fopen(strcat(unpacked_dir, "/", name, ".sws"), "r");
    fseek(fl, 0, SEEK_END);
    n_2 = ftell(fl) / 12;
    frewind(fl);
    epri.field = fread(fl, n_2, "double");
    epri.sync = double(fread(fl, n_2, "float"));
    fclose(fl);
    
    total = epri.sets.n * epri.sets.nScans * epri.sets.k3;
    fl = fopen(strcat(unpacked_dir, "/", name, ".raw"), "r");
    epri.raw = fread(fl, total, "float");
    epri.raw = reshape(epri.raw, [epri.sets.n epri.sets.nScans * epri.sets.k3]);
    fclose(fl);
    
    epri.prj0 = double(epri.raw);
    epri.prj0(:, 2 : 2 : end) = flip(epri.prj0(:, 2 : 2 : end));
    epri.prj0 = reshape(epri.prj0, [epri.sets.n epri.sets.nScans epri.sets.k3]);
    epri.prj0 = squeeze(mean(epri.prj0, 2));
    
  else
    fl = fopen(strcat(unpacked_dir, "/", name, ".prj"), "r");
    epri.prj0 = fread(fl, Inf, "double");
    epri.prj0 = reshape(epri.prj0, [epri.sets.n epri.sets.k3]);
    fclose(fl);
  endif
  
  fl = fopen(strcat(unpacked_dir, "/", name, ".xyz"), "r");
  epri.xyz = fread(fl, Inf, "double");
  epri.xyz = reshape(epri.xyz, [epri.sets.k3, 3]);
  fclose(fl);

  fl = fopen(strcat(unpacked_dir, "/", name, ".frq"), "r");
  epri.frq = fread(fl, Inf, "double");
  fclose(fl);

endfunction
##
