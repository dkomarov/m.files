function sml = cnv_Voigt(field, peak)  # inaccurate & slow, for test purposes only
 
  if ~isfield(peak, "phase")
    peak.phase = 0;
  endif
 
  if ~isfield(peak, "intensity")
    peak.intensity = 1;
  endif
  
  if ~isfield(peak, "offset")
    peak.offset = 0;
  endif

  sr2 = sqrt(2);
  srp = sqrt(pi);
  
  n = size(field, 1);
  sw = (field(n) - field(1)) * n / (n - 1);
  cf = field(n / 2 + 1);

  iota = (- n / 2  : n / 2- 1)';
  w = peak.gs / sw * n;
  
  Absorption = 1 / pi * (peak.lr ./ ((field - peak.x0) .^ 2 + peak.lr ^ 2));
  Dispersion = 1 / pi * ((field - peak.x0) ./ ((field - peak.x0) .^ 2 + peak.lr ^ 2));
  Lorentzian = cos(peak.phase) * Absorption - sin(peak.phase) * Dispersion;
  
  sml = zeros(n, 1);
  
  if ~isfield(peak, "mode") || strcmpi(peak.mode, 'd') || strcmpi(peak.mode, "derivative")
    dGaussian = iota / (w ^ 3 * sr2 * srp) .* exp(-0.5 * iota .^ 2 / w ^ 2) * n / sw;
    dGaussian = fftshift(dGaussian);
    
    for i = 1 : n
      sml(i) = circshift(dGaussian, i - 1)' * Lorentzian;
    endfor
    
  elseif strcmpi(peak.mode, 'a') || strcmpi(peak.mode, "absorption")
    Gaussian = 1 / (w * sr2 * srp) * exp(-0.5 * iota .^ 2 / w ^ 2);
    Gaussian = fftshift(Gaussian);
    
    for i = 1 : n
      sml(i) = circshift(Gaussian, i - 1)' * Lorentzian;
    endfor
    
  else
    error("cnv_Voigt:mode_str", "Unknown lineshape: absorption | derivative.")    
  endif
  
  sml = sml * peak.intensity + peak.offset;
  
endfunction
##
