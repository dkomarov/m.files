// pr3dx.cc v1.1_oct by DK, December 2019
//
// This OCT-function computes EPR projections of a 3D image.
// Usage: prjs = pr3dx(img, prj0, xyz, spc);
//

#include <complex.h>
#include <fftw3.h>
#include <octave/oct.h>

DEFUN_DLD(pr3dx, args, nargout,
  "Compute EPR projections of a 3D image.\n"
  "Usage: prjs = pr3dx(img, prj0, xyz, spc);\n")
{
  if (args.length() != 4)
    error("Not enough input arguments. Usage: prjs = pr3dx(img, prj0, xyz, spc);\n");
  
  Matrix prjs(args(1).matrix_value().dims(), 0.0);
  
  const NDArray &img = args(0).array_value();
  const Matrix &xyz = args(2).matrix_value();
  const ColumnVector &spc = args(3).column_vector_value();
  
  dim_vector dims = img.dims();
  size_t n = prjs.dim1();
  size_t k3 = prjs.dim2();
  size_t nn = spc.dim1();
  
  if (img.ndims() != 3)
    error("Image matrix must be a three-dimensional array, img(x, y, z).\n");
  
  if (xyz.dim1() != k3)
    error("Number of projections must be equal to the number of gradients.\n");

  if (xyz.dim2() != 3)
    error("Gradients must have three spatial components, [grx gry grz].\n");
  
  if (nn < n)
    error("EPR spectrum must be of equal length or longer than projections.\n");
  
  if (nn % 2 || n % 2)
    error("Current algorithm works only for even-sized projections.\n");

  size_t i, j, ix, iy, iz;
  double grx, gry, grz, r, r_max = 0.0;
  double origin, z_shift, yz_shift;
  double c, flr;
    
  ColumnVector spatial(nn);
  ColumnVector pr_(nn);
  
  const double *imgData;
  double *prData;

//******************************************************************************   
// Check projection size
  
  for (j = 0; j < k3; ++j)
  {
    grx = fabs(xyz(j, 0));
    gry = fabs(xyz(j, 1));
    grz = fabs(xyz(j, 2));
    
    r = (dims(0) - 1) * grx + (dims(1) - 1) * gry + (dims(2) - 1) * grz;
    
    if (r > r_max)
      r_max = r;
  }
  
  r_max = (nn + r_max) / 2.0;   // cct, FFT-index
  
  if ((size_t)r_max + 1 > nn - 1)
    error("Zero-gradient projection is too short. Dmax = %.2f\n"
          "Consider appending with zeros or decrease field-of-view.\n", r_max * 2 - nn);
  
//******************************************************************************   
// FFTW prerequisite
  
  double *rl;
  fftw_complex *cx;
  
  size_t rlDataBytes = sizeof(double) * nn;
  size_t cxDataBytes = sizeof(fftw_complex) * nn;
  size_t cxMeaningful = sizeof(fftw_complex) * (nn / 2 + 1);
  
  rl = (double *)fftw_malloc(rlDataBytes);
  cx = (fftw_complex *)fftw_malloc(cxDataBytes);
  
  fftw_plan fft, ifft;
  fft = fftw_plan_dft_r2c_1d(nn, rl, cx, FFTW_MEASURE);   // try FFTW_PATIENT |
  ifft = fftw_plan_dft_c2r_1d(nn, cx, rl, FFTW_MEASURE);  // FFTW_DESTROY_INPUT
  
  ComplexColumnVector ftSpt(nn, 0.0 + 0.0 * I);
  ComplexColumnVector ftSpc(nn, 0.0 + 0.0 * I);
  ComplexColumnVector ftPr_(nn, 0.0 + 0.0 * I);

  memcpy(rl, spc.fortran_vec(), rlDataBytes);
  fftw_execute(fft);
  memcpy(ftSpc.fortran_vec(), cx, cxMeaningful);
  
//******************************************************************************   
// Main computation loop
  
  for (j = 0; j < k3; ++j)
  {
    grx = xyz(j, 0);
    gry = xyz(j, 1);
    grz = xyz(j, 2);

    origin = (nn + (dims(0) - 1) * grx +
      (dims(1) - 1) * gry + (dims(2) - 1) * grz) / 2.0;
    
    spatial.fill(0.0);
    prData = spatial.fortran_vec();
    imgData = img.fortran_vec();
    
    for (iz = 0; iz < dims(2); ++iz)
    {
      z_shift = origin - iz * grz;
      
      for (iy = 0; iy < dims(1); ++iy)
      {
        yz_shift = z_shift - iy * gry;
        
        for (ix = 0; ix < dims(0); ++ix)
        {
          r = yz_shift - ix * grx;
          c = modf(r, &flr);
          
          *(prData + (size_t)flr) += *imgData * (1.0 - c);
          *(prData + (size_t)flr + 1) += *imgData++ * c;
        }
      }
    }
    
    memcpy(rl, spatial.fortran_vec(), rlDataBytes);
    fftw_execute(fft);
    memcpy(ftSpt.fortran_vec(), cx, cxMeaningful);
    
    for (i = 0; i < nn / 2 + 1; ++i)
      ftPr_(i) = ftSpt(i) * ftSpc(i);
    
    memcpy(cx, ftPr_.fortran_vec(), cxDataBytes);
    fftw_execute(ifft);
    memcpy(pr_.fortran_vec(), rl, rlDataBytes);
    
    prjs.insert(pr_.extract_n(0, n / 2), n / 2, j);
    prjs.insert(pr_.extract_n(nn - n / 2, n / 2), 0, j);
    
    if (octave_signal_caught)
      goto exit;
  }
     
  exit:
  
  fftw_destroy_plan(fft);
  fftw_destroy_plan(ifft);
  
  fftw_free(rl);
  fftw_free(cx);
  
  // fftw_cleanup();    
  // DEBUG ME: may cause crashes for unkonwn reason
  
  prjs /= nn;   // FFT <-> iFFT normalization
  
  return octave_value(prjs);
}
//
