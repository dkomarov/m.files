// art3dx.cc v1.2_oct by DK, January 2020
// 
// This OCT-function reconstructs 3D spatial EPR images
// using zero-gradient projection as (de)convolution kernel. 
//
// Usage: img = art3dx(img0, prjs, xyz, spc); 
// Arguments:
//     img0(x, y, z) - 3D map of integral signal intensity. Could be an image
//     from previous iteration or zero-filled matrix for the first iteration.
// 
//     prjs(n, k3) - array of EPR projections. 
//     n - number of data points; k3 - total number of projections.
// 
//     xyz(k3, 3) - normalized gradients of magetic field.
//     xyz = Gradient * FOV / matrix_size * n / sweep_width.
// 
//     spc - zero-gradient projection or EPR spectrum of the probe.
//     The spectrum can be longer than projections in order to include
//     any additional EPR peaks from outside the sweep width,
//     but it must have the same sampling rate as the projections.
//

#include <complex.h>
#include <fftw3.h>
#include <octave/oct.h>

DEFUN_DLD(art3dx, args, nargout,
  "Algebraic reconstruction of a 3D spatial image.\n"
  "Usage: img = art3dx(img0, prjs, xyz, spc);\n")
{
  if (args.length() != 5)
    error("Not enough input arguments. Usage: img = art3dx(img0, prjs, xyz, spc, ld);\n");
  
  NDArray img = args(0).array_value();
  const Matrix &prjs = args(1).matrix_value();
  const Matrix &xyz = args(2).matrix_value();
  const ColumnVector &spc = args(3).column_vector_value();
  const double ld = args(4).double_value();
    
  dim_vector dims = img.dims();
  size_t n = prjs.dim1();
  size_t k3 = prjs.dim2();
  size_t nn = spc.dim1();
  
  if (img.ndims() != 3)
    error("Image matrix must be a three-dimensional array, img0(x, y, z).\n");
  
  if (xyz.dim1() != k3)
    error("Number of projections must be equal to the number of gradients.\n");

  if (xyz.dim2() != 3)
    error("Gradients must have three spatial components, [grx gry grz].\n");
  
  if (nn < n)
    error("EPR spectrum must be of equal length or longer than projections.\n");
  
  if (nn % 2 || n % 2)
    error("Current algorithm works only for even-sized projections.\n");

  size_t i, j, ix, iy, iz;
  double grx, gry, grz, r, r_max = 0;
  double origin, z_shift, yz_shift;
  double sc, absGr, d2, wt, c, flr;
    
  ColumnVector spt(nn);
  ColumnVector pr_(nn);
  ColumnVector crl(nn);   // used for both: auto- and cross-correlation
  ColumnVector iwt(nn);

  double *imgData, *prData;

//******************************************************************************
// Checking projection size
  
  for (j = 0; j < k3; ++j)
  {
    grx = fabs(xyz(j, 0));
    gry = fabs(xyz(j, 1));
    grz = fabs(xyz(j, 2));
    
    r = (dims(0) - 1) * grx + (dims(1) - 1) * gry + (dims(2) - 1) * grz;
    
    if (r > r_max)
      r_max = r;
  }
  
  r_max = (nn + r_max) / 2.0;   // cct, FFT-index
  
  if ((size_t)r_max + 1 > nn - 1)
    error("Zero-gradient projection is too short. Dmax = %.2f\n"
          "Consider appending with zeros or decrease field-of-view.\n", r_max * 2 - nn);
  
//******************************************************************************   
// FFTW prerequisite
  
  double *rl;
  fftw_complex *cx;
  
  size_t rlDataBytes = sizeof(double) * nn;
  size_t cxDataBytes = sizeof(fftw_complex) * nn;
  size_t cxMeaningful = sizeof(fftw_complex) * (nn / 2 + 1);
  
  rl = (double *)fftw_malloc(rlDataBytes);
  cx = (fftw_complex *)fftw_malloc(cxDataBytes);
  
  fftw_plan fft, ifft;
  fft = fftw_plan_dft_r2c_1d(nn, rl, cx, FFTW_MEASURE);   // try FFTW_PATIENT |
  ifft = fftw_plan_dft_c2r_1d(nn, cx, rl, FFTW_MEASURE);  // FFTW_DESTROY_INPUT
  
  ComplexColumnVector ftSpt(nn, 0.0 + 0.0 * I);
  ComplexColumnVector ftSpc(nn, 0.0 + 0.0 * I);
  ComplexColumnVector ftPr_(nn, 0.0 + 0.0 * I);
  ComplexColumnVector ftCrl(nn, 0.0 + 0.0 * I);
  
  memcpy(rl, spc.fortran_vec(), rlDataBytes);
  fftw_execute(fft);
  memcpy(ftSpc.fortran_vec(), cx, cxMeaningful);
  
  for (i = 0; i < nn / 2 + 1; ++i)
    ftCrl(i) = ftSpc(i) * conj(ftSpc(i));
  
  memcpy(cx, ftCrl.fortran_vec(), cxDataBytes);
  fftw_execute(ifft);
  memcpy(crl.fortran_vec(), rl, rlDataBytes);
  
  sc = 0;
  for (i = 0; i < nn; ++i)
    sc += crl(i) * crl(i);
  
  sc = crl(0) / sc;   // nn normalization is not needed here
  
  absGr = sqrt(xyz(0, 0) * xyz(0, 0) +
    xyz(0, 1) * xyz(0, 1) + xyz(0, 2) *xyz(0, 2));
  // IMPORTANT: all gradients must be of equal magnitude
  
  c = M_PI / (absGr * absGr * absGr);
  d2 = dims(0) * dims(0) + dims(1) * dims(1) + dims(2) * dims(2);
  
  for (i = 0; i < nn; ++i)
  {
    r = (double)i - (nn / 2);
    wt = c * (d2 * absGr * absGr / 4.0 - r * r);
    
    if (wt < 1.0)
      wt = 1.0;   // to avoid division by zero
    
    iwt(i) = sc / (wt * ld);
  }
  
//******************************************************************************   
// Main reconstruction loop
  
  for (j = 0; j < k3; ++j)
  {
    grx = xyz(j, 0);
    gry = xyz(j, 1);
    grz = xyz(j, 2);

    origin = (nn + (dims(0) - 1) * grx +
      (dims(1) - 1) * gry + (dims(2) - 1) * grz) / 2.0;
    
    spt.fill(0.0);
    prData = spt.fortran_vec();
    imgData = img.fortran_vec();
    
    for (iz = 0; iz < dims(2); ++iz)
    {
      z_shift = origin - iz * grz;
      
      for (iy = 0; iy < dims(1); ++iy)
      {
        yz_shift = z_shift - iy * gry;
        
        for (ix = 0; ix < dims(0); ++ix)
        {
          r = yz_shift - ix * grx;
          c = modf(r, &flr);
          
          *(prData + (size_t)flr) += *imgData * (1.0 - c);
          *(prData + (size_t)flr + 1) += *imgData++ * c;
        }
      }
    }
    
    memcpy(rl, spt.fortran_vec(), rlDataBytes);
    fftw_execute(fft);
    memcpy(ftSpt.fortran_vec(), cx, cxMeaningful);
    
    for (i = 0; i < nn / 2 + 1; ++i)
      ftPr_(i) = ftSpt(i) * ftSpc(i);
    
    memcpy(cx, ftPr_.fortran_vec(), cxDataBytes);
    fftw_execute(ifft);
    memcpy(pr_.fortran_vec(), rl, rlDataBytes);

    pr_.fill(0.0, n / 2, nn - n / 2 - 1);
    
    for (i = 0; i < n / 2; ++i)
    {
      pr_(i) = prjs(n / 2 + i, j) - pr_(i) / nn;
      pr_(nn - n / 2 + i) = prjs(i, j) - pr_(nn - n / 2 + i) / nn;
    }
 
    memcpy(rl, pr_.fortran_vec(), rlDataBytes);
    fftw_execute(fft);
    memcpy(ftPr_.fortran_vec(), cx, cxMeaningful);
    
    for (i = 0; i < nn / 2 + 1; ++i)
      ftCrl(i) = ftPr_(i) * conj(ftSpc(i));

    memcpy(cx, ftCrl.fortran_vec(), cxDataBytes);
    fftw_execute(ifft);
    memcpy(crl.fortran_vec(), rl, rlDataBytes);
    
    for (i = 0; i < nn; ++i)
      crl(i) *= iwt(i);      // nn normalization is canceled by sc
    
    prData = crl.fortran_vec();
    imgData = img.fortran_vec();
    
    for (iz = 0; iz < dims(2); ++iz)
    {
      z_shift = origin - iz * grz;
      
      for (iy = 0; iy < dims(1); ++iy)
      {
        yz_shift = z_shift - iy * gry;
        
        for (ix = 0; ix < dims(0); ++ix)
        {
          r = yz_shift - ix * grx;
          c = modf(r, &flr);
          
          *imgData += *(prData + (size_t)flr) * (1.0 - c);
          *imgData++ += *(prData + (size_t)flr + 1) * c;
        }
      }
    }
    
    if (octave_signal_caught)
      goto exit;
  }

  exit:
  
  fftw_destroy_plan(fft);
  fftw_destroy_plan(ifft);
  
  fftw_free(rl);
  fftw_free(cx);
  
  // fftw_cleanup(); 
  // DEBUG ME: may cause crashes (interferes with Octave?)
  
  return octave_value(img);
}
//
