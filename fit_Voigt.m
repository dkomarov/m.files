function [sml, peak] = fit_Voigt(varargin)
  
  if nargin == 1
    spc = varargin{1};
    guess = find_peak(spc, "absorption");
    
  elseif nargin == 2
    spc = varargin{1};
    guess = varargin{2};
    
  else
    error("dVoigt:nargout", "Incorrect number of input arguments.")    
  endif
  
  sr2 = sqrt(2);
  srp = sqrt(pi);
  
  Voigt = @(gs, w) w / (gs * sr2 * srp);
  dVx0 = @(gs, z, w) (z .* w * srp - 1i) / (gs ^ 2 * pi);
  dVgs = @(gs, z, w) (z .* (z .* w - 1i / srp) - 0.5 * w) * sr2 / (gs ^ 2 * srp);
  dVlr = @(gs, z, w) (-1 - 1i * z .* w * srp) / (gs ^ 2 * pi);

  field = spc(:, 1);
  signal = spc(:, 2);
  n = size(field, 1);
  
  B = [guess.x0; guess.gs; guess.lr; guess.phase; guess.intensity; guess.offset];

  phasor = exp(1i * B(4));
  z = (field - B(1) + 1i * B(3)) / (B(2) * sr2);
  w = hui(z);
  
  sml = real(phasor * Voigt(B(2), w)) * B(5) + B(6);
  R2 = sum((signal - sml) .^ 2);
  
  J = zeros(n, 6);
  
  J(:, 1) = real(phasor * dVx0(B(2), z, w)) * B(5);
  J(:, 2) = real(phasor * dVgs(B(2), z, w)) * B(5);
  J(:, 3) = real(phasor * dVlr(B(2), z, w)) * B(5);
  J(:, 4) = - imag(phasor * Voigt(B(2), w)) * B(5);
  J(:, 5) = real(phasor * Voigt(B(2), w));
  J(:, 6) = ones(n, 1);

  JJ = J' * J;
  
  ld = 1;
  
  for itr = 1 : 100
    D = (JJ + ld * diag(diag(JJ))) \ J' * (signal - sml);
    
    if (B(2) + D(2)) < 0
      D(2) = -0.5 * B(2);
    endif
    
    if (B(3) + D(3)) < 0
      D(3) = -0.5 * B(3);
    endif
    
    tolerance = sum((D ./ B) .^ 2);
    if tolerance < 1e-33    % double precision squared
      break
    endif

    _sml = sml;
    _R2 = R2;
    B = B + D;
    
    phasor = exp(1i * B(4));
    z = (field - B(1) + 1i * B(3)) / (B(2) * sr2);
    w = hui(z);

    sml = real(phasor * Voigt(B(2), w)) * B(5) + B(6);
    R2 = sum((signal - sml) .^ 2);
    
    if R2 < _R2
      ld = ld / 2;
        
      J(:, 1) = real(phasor * dVx0(B(2), z, w)) * B(5);
      J(:, 2) = real(phasor * dVgs(B(2), z, w)) * B(5);
      J(:, 3) = real(phasor * dVlr(B(2), z, w)) * B(5);
      J(:, 4) = - imag(phasor * Voigt(B(2), w)) * B(5);
      J(:, 5) = real(phasor * Voigt(B(2), w));
      J(:, 6) = ones(n, 1);

      JJ = J' * J;
    else
        ld = ld * 10;
        B = B - D;
        sml = _sml;
        R2 = _R2;
    endif
  endfor

  if itr == 100
    warning("dVoigt:convergence", "Spectrum fitting did not converge.")
  endif

  if B(5) < 0
    B(4) += pi;
    B(5) *= -1;
  endif
  
  B(4) = mod(B(4), 2 * pi);
  if B(4) > pi
    B(4) -= 2 * pi;
  endif
  
  peak.mode = "absorption";
  peak.x0 = B(1);
  peak.gs = B(2);
  peak.lr = B(3);
  peak.phase = B(4);
  peak.intensity = B(5);
  peak.offset = B(6);
  peak.itr = itr;

endfunction

## Tested, works well. Jacobian correct.
## DK, May 2024.
