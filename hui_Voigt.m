function sml = hui_Voigt(field, peak)  # peak: (mode) x0 gs lr (phase) (intensity) (offset)
  
  if ~isfield(peak, "phase")
    peak.phase = 0;
  endif
  
  if ~isfield(peak, "intensity")
    peak.intensity = 1;
  endif

  if ~isfield(peak, "offset")
    peak.offset = 0;
  endif

  sr2 = sqrt(2);
  srp = sqrt(pi);

  z = (field - peak.x0 + 1i * peak.lr) / (peak.gs * sr2);
  w = hui(z);
  
  if ~isfield(peak, "mode") || strcmpi(peak.mode, 'd') || strcmpi(peak.mode, "derivative")
    sml = real(exp(1i * peak.phase) * (1i / srp - z .* w)) / (peak.gs ^ 2 * srp);
    
  elseif strcmpi(peak.mode, 'a') || strcmpi(peak.mode, "absorption")
    sml = real(exp(1i * peak.phase) * w) / (peak.gs * sr2 * srp);
    
  else
    error("hui_Voigt:mode_str", "Unknown lineshape: absorption | derivative.")    
  endif
  
  sml = sml * peak.intensity + peak.offset;

endfunction

## Tested, works well.
## DK, May 2024.
