function dd = gs_diff(data, width)
  
  [n, m] = size(data);
  v = (-n : n - 1)';
  
  if width == 0
    dgs = 1i * v;
  else
    fw = n / pi / width;    %cct
    dgs = 1i * v .* exp(-0.5 * v .^ 2 / fw ^ 2);
  endif
  
  dgs = fftshift(dgs) * pi / n;
  dataatad = [data; flip(data)];
  fdd = fft(dataatad) .* (dgs * ones(1, m));
  dd = real(ifft(fdd))(1 : n, :);
  
endfunction

## Thoroughly tested, correct.
## DK, May 2024.
