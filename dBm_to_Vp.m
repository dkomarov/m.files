function Vp = dBm_to_Vp(dBm)
  
  Vp = 10 .^ (dBm / 20 - 0.5);
  
endfunction
##
