function field = frq2field(frq)
  
  g_3CP = 2.0086;
  bohr = 9274008.99;
  planck = 6.62606896;
  
  field = frq * planck / (g_3CP * bohr);
  
endfunction
##
