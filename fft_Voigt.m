function sml = fft_Voigt(field, peak)  # peak: (mode) x0 gs lr (phase) (intensity) (offset)
  
  if ~isfield(peak, "phase")
    peak.phase = 0;
  endif

  if ~isfield(peak, "intensity")
    peak.intensity = 1;
  endif

  if ~isfield(peak, "offset")
    peak.offset = 0;
  endif

  n = size(field, 1);
  sw = (field(n) - field(1)) * n / (n - 1);
  cf = field(n / 2 + 1);
  
  iota = (- n : n - 1)';
  _field_ = iota / n * sw + cf;
  
  fw = sw / pi / peak.gs;
  
  Absorption = 1 / pi * (peak.lr ./ ((_field_ - peak.x0) .^ 2 + peak.lr ^ 2));
  Dispersion = 1 / pi * ((_field_ - peak.x0) ./ ((_field_ - peak.x0) .^ 2 + peak.lr ^ 2));
  Lorentzian = cos(peak.phase) * Absorption - sin(peak.phase) * Dispersion;
  
  if ~isfield(peak, "mode") || strcmpi(peak.mode, 'd') || strcmpi(peak.mode, "derivative")
    ftdGaussian = fftshift(1i * pi * iota / sw .* exp(-0.5 * iota .^ 2 / fw ^ 2));
    sml = real(ifft(fft(Lorentzian) .* ftdGaussian))(n / 2 + 1 : n / 2 * 3);
    
  elseif strcmpi(peak.mode, 'a') || strcmpi(peak.mode, "absorption")
    ftGaussian = fftshift(exp(-0.5 * iota .^ 2 / fw ^ 2));
    sml = real(ifft(fft(Lorentzian) .* ftGaussian))(n / 2 + 1 : n / 2 * 3);
    
  else
    error("fft_Voigt:mode_str", "Unknown lineshape: absorption | derivative.")    
  endif
  
  sml = sml * peak.intensity + peak.offset;
  
endfunction

## Tested, works well.
## DK, May 2024.
