##  FULLY VERIFIED, CCT
##  DK, July 2023

function Znew = append_element(frq, Z, element)
  
  n = size(frq, 1);
  
  if size(Z, 1) ~= n
    error("Please provide frequency range for given Z")
  endif
  
  m = 367;            % prime number
  c = 299792458;      % speed of light
  w = 2 * pi * frq;

  lh = (1 : m) / m;
  hl = 1 ./ lh;
  
  Zlast = Z(:, end) * ones(1, m);
  
  switch element.type
    case "R_serial"
      R = element.value * ones(n, 1) * lh;
      Znew = cat(2, Z,  Zlast + R);
      
    case "R_parallel"
      R = element.value * ones(n, 1) * hl ;
      Znew = cat(2, Z,  Zlast .* R ./ (Zlast + R));
      
    case "L_serial"
      X = 1i * w * element.value * lh;
      Znew = cat(2, Z,  Zlast + X);
      
    case "L_parallel"
      X = 1i * w * element.value * hl;
      Znew = cat(2, Z,  Zlast .* X ./ (Zlast + X));

    case "C_serial"
      X = -1i ./ (w * element.value * hl);
      Znew = cat(2, Z,  Zlast + X);
      
    case "C_parallel"
      X = -1i ./ (w * element.value * lh);
      Znew = cat(2, Z,  Zlast .* X ./ (Zlast + X));
      
    case "transmission_line"                         
      a = log(10) / 20 * element.attenuation;        % dB / m    VERIFIED, CCT
      b = w / (c * element.propagation);             % fraction of light speed
      gl = (a + 1i * b) * element.length;            % physical length, meters, can be negative
      
      Zline = element.impedance * (Zlast + element.impedance * tanh(gl * lh)) ...
                              ./ (element.impedance + Zlast .* tanh(gl * lh));
      Znew = cat(2, Z, Zline);

    case "short_serial_stub"
      a = log(10) / 20 * element.attenuation;
      b = w / (c * element.propagation);
      gl = (a + 1i * b) * element.length;
      
      Zstub = element.impedance * tanh(gl * lh);
      Znew = cat(2, Z,  Zlast + Zstub);
      
    case "short_parallel_stub"
      a = log(10) / 20 * element.attenuation;
      b = w / (c * element.propagation);
      gl = (a + 1i * b) * element.length;
      
      Zstub = element.impedance * tanh(gl * lh);
      Znew = cat(2, Z,  Zlast .* Zstub ./ (Zlast + Zstub));
      
    case "open_serial_stub"
      a = log(10) / 20 * element.attenuation;
      b = w / (c * element.propagation);
      gl = (a + 1i * b) * element.length;
      
      Zstub = element.impedance ./ tanh(gl * lh);
      Znew = cat(2, Z,  Zlast + Zstub);
      
    case "open_parallel_stub"
      a = log(10) / 20 * element.attenuation;
      b = w / (c * element.propagation);
      gl = (a + 1i * b) * element.length;
      
      Zstub = element.impedance ./ tanh(gl * lh);
      Znew = cat(2, Z,  Zlast .* Zstub ./ (Zlast + Zstub));
      
    otherwise
      error("Unknown element \"%s\"", element.type)
  endswitch
endfunction
#
      
