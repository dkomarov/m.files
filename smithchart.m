function smithchart(varargin)
  
  if nargin == 1
    adm = varargin{1};
  else
    adm = 0;
  endif
  
  n = 367;      % prime number
  circ = exp((0 : n)' / n * 2 * pi * 1i);
  gridcolor = [0.8 0.8 0.9];
  adm_gridcolor = [1 0.9 0.9];

  hold on
    
  plot(circ * 5 / 6 + 1 / 6, 'color', gridcolor)
  plot(circ * 2 / 3 + 1 / 3, 'color', gridcolor)
  plot(circ * 1 / 2 + 1 / 2, 'color', gridcolor)
  plot(circ * 1 / 3 + 2 / 3, 'color', gridcolor)
  plot(circ * 1 / 6 + 5 / 6, 'color', gridcolor)

  plot([-1; 1], [0; 0], 'color', gridcolor)
  
  sub = abs(circ * 5 + 1 + 1i * 5) <= 1;
  plot(circ(sub) * 5 + 1 + 1i * 5, 'color', gridcolor)
  plot(conj(circ(sub) * 5 + 1 + 1i * 5), 'color', gridcolor)

  sub = abs(circ * 2 + 1 + 1i * 2) <= 1;
  plot(circ(sub) * 2 + 1 + 1i * 2, 'color', gridcolor)
  plot(conj(circ(sub) * 2 + 1 + 1i * 2), 'color', gridcolor)
    
  sub = abs(circ + 1 + 1i) < 1;
  plot(circ(sub) + 1 + 1i, 'color', gridcolor)
  plot(conj(circ(sub) + 1 + 1i), 'color', gridcolor)
    
  sub = abs(circ * 1 / 2 + 1 + 1i / 2) <= 1;
  plot(circ(sub) * 1 / 2 + 1 + 1i / 2, 'color', gridcolor)
  plot(conj(circ(sub) * 1 / 2 + 1 + 1i / 2), 'color', gridcolor)
    
  sub = abs(circ * 1 / 5 + 1 + 1i / 5) <= 1;
  plot(circ(sub) * 1 / 5 + 1 + 1i / 5, 'color', gridcolor)
  plot(conj(circ(sub) * 1 / 5 + 1 + 1i / 5), 'color', gridcolor)
  
  if adm
    plot(circ * 5 / 6 - 1 / 6, 'color', adm_gridcolor)
    plot(circ * 2 / 3 - 1 / 3, 'color', adm_gridcolor)
    plot(circ * 1 / 2 - 1 / 2, 'color', adm_gridcolor)
    plot(circ * 1 / 3 - 2 / 3, 'color', adm_gridcolor)
    plot(circ * 1 / 6 - 5 / 6, 'color', adm_gridcolor)
  endif
  
  plot(circ, 'color', [0 0 0])
  
  set(gca,'visible','off')
  drawnow

endfunction
#