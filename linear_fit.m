function varargout = linear_fit(varargin)
  
  switch nargin
    case 1
      y = varargin{1};
      n = length(y);
      x = (1 : n)';
    case 2
      x = varargin{1};
      y = varargin{2};
      n = length(y);
  endswitch
  
  X = sum(x);
  Y = sum(y);
  XX = sum(x .* x);
  XY = sum(x .* y);
  
  a = (n * XY - X * Y) / (n * XX - X * X);
  b = (Y * XX - X * XY) / (n * XX - X * X);

  switch nargout
    case 0
      plot(x, y)
      hold on
      plot(x, a * x + b)
      hold off
    case 1
      varargout{1} = a * x + b;
    case 2
      varargout{1} = a;
      varargout{2} = b;
  endswitch
  
endfunction
##
