function preview3d(varargin)
  
  img = varargin{1};
  
  m = size(img, 1);
  mx = max(img(:));
  
  iso = mx * 0.25;
  cut = floor(size(img) / 2) + 1;

  if nargin == 2
    if isscalar(varargin{2})
      iso = varargin{2};
    else
      cut = varargin{2};
    endif
  elseif nargin == 3
    if isscalar(varargin{2}) && numel(varargin{3} == 3)
      iso = varargin{2};
      cut = varargin{3};
    elseif numel(varargin{2} == 3) && isscalar(varargin{3})
      cut = varargin{2};
      iso = varargin{3};
    endif
  endif
  
  xy = img(:, :, cut(3))';
  yz = squeeze(img(cut(1), :, :));
  xz = squeeze(img(:, cut(2), :));
  xz = flip(xz', 1);
  
  img = permute(img, [1 3 2]);
  img = flip(img, 1);
  img = flip(img, 3);
  
  vf = isosurface(img, iso);
  vn = isonormals(img, vf.vertices);
  cl = [0.57 0.66 0.69];

  clf
  set(gcf, "position", [680 60 900 900])  
  
  subplot(2, 2, 1)
  view(3)
##  view(-22.5, 30)

  daspect([1 1 1])
  axis([1 m 1 m 1 m])
  
##  tick = [-10 -5 0 5 10] / 25 * m + (m + 1) / 2;
##  ticklabel = {"-10" "-5" "0" "5" "10"};
##  
##  set(gca, "xtick", tick, "xticklabel", ticklabel)
##  set(gca, "ytick", tick, "yticklabel", ticklabel)
##  set(gca, "ztick", tick, "zticklabel", ticklabel)

  patch(vf, "FaceColor", cl, "EdgeColor", "none", "VertexNormals", vn, ...
        "SpecularStrength", 0.3, "FaceLighting", "gouraud")

  camlight("left")
  title("3D Render")

################################################################################
## Show slice cuts if needed
##
##  hold on
##  
##  sl1 = slice(img,  [], m + 1 - cut(1), []);    ## 1, 2 <=> Y, X
##  set(sl1, "linestyle", "none", "facealpha", 0.2)
##
##  sl2 = slice(img, [], [], m + 1 - cut(2));
##  set(sl2, "linestyle", "none", "facealpha", 0.2)
##  
##  sl3 = slice(img, cut(3), [], []);             ## 1, 2 <=> Y, X
##  set(sl3, "linestyle", "none", "facealpha", 0.2)
##
##  hold off
################################################################################
  
  subplot(2, 2, 2)
  imagesc(xy)
  caxis([0 mx])
  title("Front-to-Back")
  
  subplot(2, 2, 3)
  imagesc(yz)
  caxis([0 mx])
  title("Right-to-Left")
  
  subplot(2, 2, 4)
  imagesc(xz)
  caxis([0 mx])
  title("Top-to-Bottom")
  
  colormap(bone(1024))
  
endfunction
##

