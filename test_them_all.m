clc
clear
close all

function [sml, J] = Jacobian(field, guess)  # column order: x0 gs lr phase int
  
  sr2 = sqrt(2);
  srp = sqrt(pi);
  n = size(field, 1);
  
  dVoigt = @(gs, z, w) (1i - z .* w * srp) / (gs ^ 2 * pi);
  d2Vx0 = @(gs, z, w) (z .* (1i / srp - z .* w) + 0.5 * w) * sr2 / (gs ^ 3 * srp);
  d2Vgs = @(gs, z, w) ((z .^ 2 - 1) .* (1i / srp - z .* w) * 2 + z .* w) / (gs ^ 3 * srp);
  d2Vlr = @(gs, z, w) (z .* (1 / srp + 1i * z .* w) - 0.5i * w) * sr2 / (gs ^ 3 * srp);

  B = [guess.x0; guess.gs; guess.lr; guess.phase; guess.intensity; guess.offset];

  phasor = exp(1i * B(4));
  
  z = (field - B(1) + 1i * B(3)) / (B(2) * sr2);
  w = hui(z);
  
  sml = real(phasor * dVoigt(B(2), z, w)) * B(5) + B(6);  # Tested, OK
  
  J = zeros(n, 6);
  
  J(:, 1) = real(phasor * d2Vx0(B(2), z, w)) * B(5);      # Tested, OK
  J(:, 2) = real(phasor * d2Vgs(B(2), z, w)) * B(5);      # Tested, OK
  J(:, 3) = real(phasor * d2Vlr(B(2), z, w)) * B(5);      # Tested, OK
  J(:, 4) = - imag(phasor * dVoigt(B(2), z, w)) * B(5);   # Tested, OK
  J(:, 5) = real(phasor .* dVoigt(B(2), z, w));           # Tested, OK
  J(:, 6) = ones(n, 1);
  
  ##############################################################################
  
  Voigt = @(gs, w) w / (gs * sr2 * srp);
  dVx0 = @(gs, z, w) (z .* w * srp - 1i) / (gs ^ 2 * pi);
  dVgs = @(gs, z, w) (z .* (z .* w - 1i / srp) - 0.5 * w) * sr2 / (gs ^ 2 * srp);
  dVlr = @(gs, z, w) (-1 - 1i * z .* w * srp) / (gs ^ 2 * pi);
  
  B = [guess.x0; guess.gs; guess.lr; guess.phase; guess.intensity; guess.offset];

  phasor = exp(1i * B(4));
  z = (field - B(1) + 1i * B(3)) / (B(2) * sr2);
  w = hui(z);
  
  sml = real(phasor * Voigt(B(2), w)) * B(5) + B(6);  # Tested, OK
  
  J = zeros(n, 6);
  
  J(:, 1) = real(phasor * dVx0(B(2), z, w)) * B(5);   # Tested, OK
  J(:, 2) = real(phasor * dVgs(B(2), z, w)) * B(5);   # Tested, OK
  J(:, 3) = real(phasor * dVlr(B(2), z, w)) * B(5);   # Tested, OK
  J(:, 4) = - imag(phasor * Voigt(B(2), w)) * B(5);   # Tested, OK
  J(:, 5) = real(phasor * Voigt(B(2), w));            # Tested, OK
  J(:, 6) = ones(n, 1);
  
endfunction
##

cf = 440;
sw = 48;
n = 1024;

iota = (- n / 2 : n / 2 - 1)';
field = iota / n * sw + cf;

peak.x0 = 445;
peak.gs = 0.5;
peak.lr = 0.7;
peak.phase = pi / 12;
peak.intensity = 1.5;
peak.offset = 0.22;
peak.mode = "absorption";

spc(:, 1) = field;
spc(:, 2) = fft_Voigt(field, peak);

noise = randn(n, 1) * std(spc(:, 2));
spc(:, 2) += noise / 2;
##spc(:, 2) += gsf(randn(n, 1) * std(spc(:, 2)) * 16, n / 4);

<<<<<<< HEAD
dVx = @(gs, z, w, phase) real(exp(1i * phase) * (1i - z .* w * srp)) / (gs ^ 2 * pi);  ## Checked, OK!!!
dVx0 = @(gs, z, w, phase) real(exp(1i * phase) * (z .* w * srp - 1i)) / (gs ^ 2 * pi);  ## Checked, OK!!!
dVgs = @(gs, z, w, phase) real(exp(1i * phase) * (z .* (z .* w - 1i / srp) - 0.5 * w)) * sr2 / (gs ^ 2 * srp);  ## Checked, OK!!!
dVlr = @(gs, z, w, phase) real(exp(1i * phase) * (-1 - 1i * z .* w * srp)) / (gs ^ 2 * pi);  ## Checked, OK!!!
dVphase = @(gs, z, w, phase) real(exp(1i * phase) * 1i * w) / (gs * sr2 * srp);  ## Checked, OK!!!
=======
tic
[sml, results] = fit_Voigt(spc);
toc
>>>>>>> 14043db8360112b2371ee0c763dc663671ad3bcd

plot([spc(:, 2), sml])
disp(results)

<<<<<<< HEAD
z = (field - peak.x0 + 1i * peak.lr) / (peak.gs * sr2);
w = hui(z);
smth = dVgs(peak.gs, z, w, peak.phase);
=======
return

################################################################################
>>>>>>> 14043db8360112b2371ee0c763dc663671ad3bcd

peak1 = peak;
peak2 = peak;

<<<<<<< HEAD
peak1.gs = peak.gs * 0.9999995;
peak2.gs = peak.gs * 1.0000005;
dgs = peak.gs * 1e-6;

plain = (fft_Voigt(field, peak2) - fft_Voigt(field, peak1)) / dgs;
=======
peak1.intensity = peak.intensity * 0.9999995;
peak2.intensity = peak.intensity * 1.0000005;
dintensity = peak.intensity * 1e-6;

plain = fft_Voigt(field, peak);
plain = (fft_Voigt(field, peak2) - fft_Voigt(field, peak1)) / dintensity;
[sml J] = Jacobian(field, peak);
>>>>>>> 14043db8360112b2371ee0c763dc663671ad3bcd

subplot(2, 1, 1)
plot([plain J(:, 5)])
subplot(2, 1, 2)
plot(J(:, 5) - plain)

return

################################################################################

