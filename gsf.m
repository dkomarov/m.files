function fdata = gsf(data, width)
  
  [n, m] = size(data);
  v = (-n : n - 1)';

  fw = n / pi / width;    %cct
  
  gs = fftshift(exp(-0.5 * v .^ 2 / fw ^ 2));
  
  dataatad = [data; flip(data)];
  
  fdd = fft(dataatad) .* (gs * ones(1, m));
  fdata = real(ifft(fdd))(1 : n, :);
  
endfunction

## Thoroughly tested, correct.
## DK, May 2024.
