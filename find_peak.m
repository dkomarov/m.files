function peak = find_peak(spc, mode_str)
  
  peak.mode = "";
  peak.x0 = 0;
  peak.gs = 0;
  peak.lr = 0;
  peak.phase = 0;
  peak.intensity = 0;
  peak.offset = 0;
  
  n = size(spc, 1);
  sw = spc(n, 1) - spc(1, 1);
  sw *= n / (n - 1);
  
  if strcmpi(mode_str, 'a') || strcmpi(mode_str, "absorption")
    peak.mode = "absorption";
    
    x = [(1 : n / 4)'; (n / 4 * 3 + 1 : n)'];
    bl = spc(:, 2)(x);
    [a, b] = linear_fit(x, bl);
    edge_fit = (1 : n)' * a + b;
   
    peak.offset = mean(edge_fit);
    abs_profile = spc(:, 2) - edge_fit;
    
  elseif strcmpi(mode_str, 'd') || strcmpi(mode_str, "derivative")
    peak.mode = "derivative";
    
    peak.offset = mean(spc(:, 2));
    abs_profile = cumsum(spc(:, 2) - peak.offset) * sw / n;
    
  else 
    error("find_peak:mode_str", "Unknown lineshape: absorption | derivative.")    
  endif
  
  [mx, imx] = max(abs_profile);
  [mn, imn] = min(abs_profile);
  
  if mx > abs(mn)
    width = sum(abs_profile > mx / 2) / n * sw;
    
    peak.x0 = spc(imx, 1);
    peak.phase = 0.0;
    peak.intensity = mx * width * 1.3;
    
  else
    width = sum(abs_profile < mn / 2) / n * sw;
    
    peak.x0 = spc(imn, 1);
    peak.phase = pi;
    peak.intensity = -1.3 * mn * width;
  endif

  peak.gs = width / 4.7;    % cct, 50%
  peak.lr = width / 4.0;    % cct, 50%

endfunction

## Tested, works reasonably well.
## DK, May 2024.
