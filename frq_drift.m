function corrected = frq_drift(epri)
  
  corrected = epri;
  
  if isempty(epri.frq)
    return
  endif
  
  sw = epri.sets.sw;
  n = epri.sets.n;
  k3 = epri.sets.k3;
  m = size(epri.frq, 1);
  
  g_3CP = 2.0086;
  bohr = 9274008.99;
  planck = 6.62606896;
  
  hgb = planck / (g_3CP * bohr);
  drift = hgb * (epri.frq - epri.frq(1)) / sw * n;
  
  df = max(epri.frq) - min(epri.frq);
  printf("Frequency drift %.2f kHz (%.2f mG)\n", df * 0.001, df * hgb * 1000)
  
  x = (0 : m - 1)' / (m - 1);
  xi = (0 : k3 - 1)' / (k3 - 1);
  drift = interp1(x, drift, xi);
  
  prj0jrp = cat(1, epri.prj0, flip(epri.prj0));
  v = fftshift((- n : n - 1)' / (n * 2));
  ftPrjs = fft(prj0jrp) .* exp(2i * pi * v * drift');
  prjs = real(ifft(ftPrjs))(1 : n, :);
  
  corrected.prj0 = prjs;
  corrected.frq = zeros(m, 1) + epri.frq(1);
  
endfunction
##
